//
//  PokemonDetailViewController.swift
//  Pokedex 2018
//
//  Created by Wilson Gabriel Ramos Bravo on 20/6/18.
//  Copyright © 2018 Wilson Gabriel Ramos Bravo. All rights reserved.
//

import UIKit

class PokemonDetailViewController: UIViewController {

    @IBOutlet weak var pokemonNameLabel: UILabel!
    @IBOutlet weak var pokemonImagenView: UIImageView!
    @IBOutlet weak var pokemonWeightLabel: UILabel!
    @IBOutlet weak var pokemonHeighLabel: UILabel!
    var pokemon:Pokemon!
    var selectedRow: Int!
    override func viewDidLoad() {
        super.viewDidLoad()
        pokemonNameLabel.text? = pokemon.name
        pokemonHeighLabel.text = "Height: " + "\(pokemon.height)"
        pokemonWeightLabel.text = "Weight: " + "\(pokemon.weight)"
        
        
        let bm = Network()
        bm.getPokemonImage(url: pokemon.sprites.defaultSprite, completionHandler: { (imageR) in
            DispatchQueue.main.async {
                self.pokemonImagenView.image = imageR
            }
            
        })
            // Do any additional setup after loading the view.
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}

