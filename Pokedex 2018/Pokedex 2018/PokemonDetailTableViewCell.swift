//
//  PokemonDetailTableViewCell.swift
//  Pokedex 2018
//
//  Created by Wilson Gabriel Ramos Bravo on 20/6/18.
//  Copyright © 2018 Wilson Gabriel Ramos Bravo. All rights reserved.
//

import UIKit

class PokemonDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var pokemonImage: UIImageView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
