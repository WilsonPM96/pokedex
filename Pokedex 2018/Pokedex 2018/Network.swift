//
//  Network.swift
//  Pokedex 2018
//
//  Created by Wilson Gabriel Ramos Bravo on 13/6/18.
//  Copyright © 2018 Wilson Gabriel Ramos Bravo. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireImage


class Network {
    func getAllPokemon(completion:@escaping ([Pokemon])->()){
        var pokemonArray:[Pokemon] = []
        let group = DispatchGroup()
        for i in 1...8{
            group.enter()
        Alamofire.request("https://pokeapi.co/api/v2/pokemon/\(i)").responseJSON { response in
            guard let data = response.data else {
                print ("Error")
                return
            }
            guard let pokemon = try? JSONDecoder().decode(Pokemon.self, from: data) else {
                print ("Error Decoding Pokemon")
                return
            }
            pokemonArray.append(pokemon)
            group.leave()
        }
    }
        
        group.notify(queue: .main){   //espera a que el grupo este vacio, todo lo que entro haya salido
            completion(pokemonArray)
        }
    }   
    
    func getPokemonImage(url: String, completionHandler: @escaping(UIImage)->()) {

        Alamofire.request(url).responseImage { response in
            if let image = response.result.value {
                completionHandler(image)
            }
        }
        }

        
    }
        



